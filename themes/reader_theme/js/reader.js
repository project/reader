(function ($, Drupal, once, drupalSettings) {

  'use strict';

  let searchVisible = false;
  let searchWrapper = null;

  Drupal.behaviors.reader = {
    attach: function (context) {

      if (drupalSettings.user.uid === 0) {
        return;
      }

      // When clicking on the home button, we'll compare the current path. In
      // case it's the same we simply switch the classes instead of loading the
      // page.
      let path = drupalSettings.path.baseUrl + drupalSettings.path.currentPath;

      // Get columns.
      let column1 = $('.column-1');
      let column2 = $('.column-2');
      let column3 = $('.column-3');

      // Get search container.
      searchWrapper = $('.column-header__collapsible.search-container');

      // Get button links to set active classes and hide/show columns.
      $(once('button-link', '.button-link', context)).on('click', function(e) {
        let type = $(this).data('button');

        // Do not reload page on home or timelines.
        if (type === 'home' && (path === '/reader' || path.indexOf('/reader/timeline') !== -1)) {
          e.preventDefault();
        }

        // Active class.
        $('.tabs-bar a').removeClass('active');
        $(this).addClass('active');

        switch (type) {
          case 'home':
            column2.css('display', 'none');
            column3.css('display', 'none');
            column1.css('display', 'flex');
            hideSearch();
            break;
          case 'read':
            column1.css('display', 'none');
            column3.css('display', 'none');
            column2.css('display', 'flex');
            hideSearch();
            break;
          case 'write':
            column1.css('display', 'none');
            column2.css('display', 'none');
            column3.css('display', 'flex');
            hideSearch();
            break;
          case 'search':
            column1.css('display', 'flex');
            column2.css('display', 'none');
            column3.css('display', 'none');
            if (!searchVisible) {
              searchVisible = true;
              searchWrapper.removeClass('collapsed');
            }
        }
      });

      $(once('document', document)).click(function() {
        if (actionLinks !== null) {
          actionLinks.hide();
        }
      });

      // Action links.
      let actionLinks = null;
      $(once('dropdown', '.status__action-bar-dropdown', context)).on('click', function (e) {
        e.stopPropagation();
        let top = $(this).offset().top;
        actionLinks = $(this).parent().children('.status__action-bar-links');
        if (actionLinks.is(':visible')) {
          actionLinks.css("bottom", "30px");
          actionLinks.hide();
        }
        else {
          $('.status__action-bar-links').hide();
          if (top < 200) {
            actionLinks.css("bottom", "auto");
          }
          actionLinks.show();
        }
      });

      // Hide messages.
      let messages = $('.drupal-messages');
      if (messages.length > 0) {
        setTimeout(function() { messages.hide('slow') }, 3000);
      }

      // Catch logout link.
      $(once('logout', '.reader-logout', context)).on('click', function(e) {
        let l = confirm(Drupal.t("Are you sure you want to logout?"));
        if (l !== true) {
          e.preventDefault();
          return false;
        }
      });

      // Catch clicks on links inside the article, apart from gallery and action
      // links, to open in a new tab or window (depending on browser).
      $(once('article-links', '.content-article a:not(.image-gallery-item,.status__action-bar-links a', context)).on('click', function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        window.open(url, '_blank');
      });

      // Show or hide actions.
      let actionsVisible = false;
      let actionsWrapper = $('.column-header__collapsible.actions-container');
      if (actionsWrapper.length > 0) {
        $(once('actions', '.column-header__button.actions')).on('click', function() {
          if (actionsVisible) {
            actionsVisible = false;
            actionsWrapper.addClass('collapsed');
          }
          else {
            actionsWrapper.removeClass('collapsed');
            actionsVisible = true;
          }
        });
      }

      if (searchWrapper.length > 0) {
        $(once('search', '.column-header__button.search')).on('click', function() {
          if (searchVisible) {
            searchVisible = false;
            searchWrapper.addClass('collapsed');
          }
          else {
            searchWrapper.removeClass('collapsed');
            searchVisible = true;
          }
        });
      }

      // Read more.
      if (drupalSettings.reader !== undefined && drupalSettings.reader.readMore) {
        readMore(drupalSettings.reader.readMoreHeight);
      }

    }
  };

  /**
   * Apply read more to posts.
   *
   * @param height
   */
  function readMore(height) {

    let closeHeight = height;
    let moreText 	= Drupal.t('Read more');
    let lessText	= Drupal.t('Close');
    let duration	= '1000';
    let easing = 'linear';

    $(once('status-content', '.status__content')).each(function() {
      if ($(this).height() > height) {
        $(this).data('fullHeight', $(this).height()).css('height', height);
        $(this).append('<a href="javascript:void(0);" class="reader-more-link closed">' + moreText + '</a>');
      }
    });

    let openSlider = function() {
      let link = $(this);
      let openHeight = (link.parent('.status__content').data('fullHeight') + 40) + 'px';
      link.parent('.status__content').animate({'height': openHeight}, {duration: duration }, easing);
      link.text(lessText).addClass('open').removeClass('closed');
      link.unbind('click', openSlider);
      link.bind('click', closeSlider);
    }

    let closeSlider = function() {
      let link = $(this);
      link.parent('.status__content').animate({'height': closeHeight}, {duration: duration }, easing);
      link.text(moreText).addClass('closed').removeClass('open');
      link.unbind('click');
      link.bind('click', openSlider);
    }

    $('.reader-more-link').bind('click', openSlider);
  }

  /**
   * Hide search.
   */
  function hideSearch() {
    if (searchVisible && searchWrapper.length > 0) {
      searchVisible = false;
      searchWrapper.addClass('collapsed');
    }
  }

})(jQuery, Drupal, once, drupalSettings);