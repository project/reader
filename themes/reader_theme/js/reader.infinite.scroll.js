(function ($) {

  'use strict';

  Drupal.behaviors.readerInfiniteScroll = {
    attach: function () {
      let $timelineColumn = $('.column-1 .scrollable .column-timeline');
      if ($('.pager__item--next a').length > 0) {
        $('.pager__items li a').addClass('use-ajax');
        $timelineColumn.infiniteScroll({
          path: '.pager__item--next a',
          append: '.content-article',
          prefill: true,
          elementScroll: '.scrollable',
          loadOnScroll: true,
          status: '.page-load-status',
          hideNav: '.pager',
          debug: false,
        });
      }

      $timelineColumn.on( 'append.infiniteScroll', function( event, response, path ) {
        //console.log('Appended: ' + path);
        Drupal.attachBehaviors();
      });
    }
  }

})(jQuery);
