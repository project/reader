<?php

namespace Drupal\Tests\reader\Functional;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests reader functionality.
 *
 * @group reader
 */
class ReaderTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'aggregator',
    'reader',
    'reader_test',
    'node',
    'block',
  ];

  /**
   * The default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'reader_theme';

  /**
   * A user with all permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * An authenticated user with reader permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $authenticatedUser;

  /**
   * The authenticated user permissions.
   *
   * @var array
   */
  protected $authenticatedUserPermissions = [
    'access reader',
    'administer nodes',
    'bypass node access',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    try {
      $this->adminUser = $this->createUser([], 'administrator', TRUE, ['pass' => 'admin']);
    }
    catch (EntityStorageException $ignored) {
    }
    try {
      $this->authenticatedUser = $this->createUser($this->authenticatedUserPermissions, 'Reader', FALSE, ['pass' => 'reader']);
    }
    catch (EntityStorageException $ignored) {
    }

    $this->drupalCreateContentType(['type' => 'page']);
  }

  /**
   * Test reader functionality.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReader() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('/reader');
    $assert_session->addressEquals('reader/user/login');
    $assert_session->responseContains('No post types found');
    $assert_session->responseContains('No channels found');
    $assert_session->responseNotContains('Logout');

    $edit = [
      'name' => 'Reader',
      'pass' => 'reader',
    ];
    $this->submitForm($edit, 'Log in', 'user-login-form');
    $assert_session->addressEquals('reader');
    $assert_session->responseContains('Logout');
    $assert_session->responseContains('Aggregator');
    $assert_session->responseContains('All feeds');
    $assert_session->responseContains('Click on a channel to start reading');
    $assert_session->responseContains('reader/add/page');
    $this->drupalGet('reader/timeline/reader/aggregator');
    $assert_session->responseContains('No items found');

    // Add source.
    $page->clickLink('Sources');
    $page->clickLink('Aggregator');
    $page->clickLink('+ Add feed');
    $feed = [
      'title[0][value]' => 'Test feed',
      'url[0][value]' => Url::fromRoute('reader.test.feed', [], ['absolute' => TRUE])->toString(),
    ];
    $this->submitForm($feed, 'Save');
    $assert_session->pageTextContains('Test feed');

    // Run cron as admin.
    $this->drupalLogout();
    $this->drupalGet('/reader');
    $edit = [
      'name' => 'administrator',
      'pass' => 'admin',
    ];
    $this->submitForm($edit, 'Log in', 'user-login-form');
    $this->drupalGet('admin/config/system/cron');
    $this->submitForm([], 'Run cron', 'system-cron-settings');
    $this->drupalGet('reader');
    $page->clickLink('Logout');

    // Login as normal user again.
    $edit = [
      'name' => 'Reader',
      'pass' => 'reader',
    ];
    $this->submitForm($edit, 'Log in', 'user-login-form');
    $this->drupalGet('reader/timeline/reader/aggregator');
    $assert_session->responseContains('All feeds - page 1');
    $assert_session->responseContains('All of us in the Open Source CMS world are asked');
    $page->clickLink('Next ›');
    $assert_session->responseContains('All feeds - page 2');
    $assert_session->responseNotContains('All of us in the Open Source CMS world are asked');
    $assert_session->responseContains('We are currently planning for Drupal core to drop support for Internet Explorer');

    // Search.
    $this->drupalGet('reader/search', ['query' => ['keyword' => 'Droptica', 'module' => 'reader']]);
    $assert_session->responseContains('Search - page 1');
    $assert_session->responseContains('Droptica: Users, Roles and Permissions');
    $assert_session->responseContains('Drupal allows you to manage access from the admin panel');
  }

}
