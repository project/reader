<?php

namespace Drupal\reader_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class ReaderTestController extends ControllerBase {

  /**
   * Routing callback: return a feed.
   */
  public function feed() {
    $content = file_get_contents(\Drupal::service('extension.list.module')->getPath('reader_test') . '/assets/rss_template.xml');
    $date1 = date('M Y', strtotime( "-1 month"));
    $date2 = date('M Y', strtotime( "-2 month"));
    $content = str_replace (["[MONTHYEAR1]", "[MONTHYEAR2]"] , [$date1, $date2], $content);
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    $response->setContent($content);
    return $response;
  }

}
