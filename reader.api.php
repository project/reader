<?php

/**
 * @file
 * Hooks specific to the reader module.
 */

use Drupal\Core\Url;

/**
 * Returns channels.
 *
 * @see \Drupal\reader\ReaderInterface::getChannels()
 *
 * @return array
 */
function hook_reader_channels() {
  return [];
}

/**
 * Returns a sources page.
 *
 * @see \Drupal\reader\ReaderInterface::getSourcesPage()
 *
 * @param $op
 *
 * @return array
 */
function hook_reader_sources($op) {
  return [];
}

/**
 * Returns the timeline.
 *
 * @param $id
 *   The channel to get the timeline for.
 * @param null $search
 *   The search keyword.
 *
 * @see \Drupal\reader\ReaderInterface::getTimeline()
 *
 * @return array[]
 */
function hook_reader_timeline($id, $search = NULL) {
  return ['items' => []];
}

/**
 * Returns actions for a timeline.
 *
 * @param $id
 *   The channel to get the actions for.
 *
 * @see \Drupal\reader\ReaderInterface::getTimelineActions()
 *
 * @return array
 */
function hook_reader_timeline_actions($id) {
  return [];
}

/**
 * Do a timeline action.
 *
 * @param $action
 *   The action id.
 * @param $id
 *   The channel id
 *
 * @return void
 */
function hook_reader_do_timeline_action($action, $id) {

}

/**
 * Returns actions for a post.
 *
 * @param $id
 *   The channel to get the actions for.
 * @param $item
 *   The post item to get the actions for.
 *
 * @see \Drupal\reader\ReaderInterface::getPostActions()
 *
 * @return array
 */
function hook_reader_post_actions($id, $item) {
  return [];
}

/**
 * Do a post action.
 *
 * @param $action
 *   The action id.
 * @param $id
 *   The channel id
 * @param $items
 *   The items to perform an action on.
 *
 * @return void
 */
function hook_reader_do_post_action($action, $id, array $items) {
  // Do something.
}

/**
 * Alter the item action links.
 *
 * @param $links
 *   The current links
 * @param $context
 *   Array with two keys:
 *     - id: id of the channel
 *     - post_id: id of the post
 */
function hook_reader_post_actions_alter(&$links, $context) {
  $links[] = [
    'url' => Url::fromRoute('reader.post.action', ['module' => 'custom', 'action' => 'move', 'id' => $context['id,'], 'post_id' => $context['post_id']]),
    'title' => 'Move',
    'attributes' => ['class' => ['post-action']],
  ];
}

/**
 * Alter the manifest.
 *
 * @param array $manifest
 *   The current manifest.
 */
function hook_reader_manifest_alter(array &$manifest) {
  $manifest['name'] = 'My awesome app';
}
