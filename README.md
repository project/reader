# Personal reader

A personal reader on your website which is installable as a PWA on your phone
or tablet.

The project consists of two parts:

- reader module: API for getting channels, streams etc. The main entry point is
at /reader. Configuration is at admin/config/services/reader.
- reader theme: theme for displaying the content. The theme enables you to
install the reader on the home screen of your mobile phone.

Modules that implement the API:

- ActivityPub: https://www.drupal.org/project/activitypub
- IndieWeb Microsub: https://www.drupal.org/project/indieweb
  Note: only works using the internal Microsub endpoint.
- Reader module on behalf of Aggregator for D9 and
  https://www.drupal.org/project/aggregator for D10.

## Install

- Enable the Reader module at Extend
- Enable the Reader theme at Appearance
- Go to the block layout of the reader theme and make sure following blocks are
  installed:
    - Main content in content
    - Reader channels in column 2
    - Reader create links in column 3
    - Reader actions in actions
    - Reader search in search
    - Drupal messages in messages
- Enable the 'access reader' permission for authenticated users.
- Configure settings at admin/config/services/reader.

## Prepopulate

Install Prepopulate module to pass data to the node form via inline actions:
- post url to link fields

https://www.drupal.org/project/prepopulate

## Images

Install the Magnific popup module so that images are shown in a gallery.

https://www.drupal.org/project/magnific_popup

## Node forms

The node forms are available on path /reader/add/{node_type}.
The module can alter the form removing all unnecessary form items like meta,
menu, preview and revision which can not be controlled through form display.

The module also ships with a 'Reader' form mode which you can enable on the
form display of every content type.

## API

Check reader.api.php and ReaderInterface for methods which are called by the
reader if you want to integrate.

## Changing css

- cd themes/reader_theme
- npm install
- npm run scss

## Sponsors

I would like to extend many thanks to the following sponsors for funding development.

- [NLnet Foundation](https://nlnet.nl) and [NGI0
  Discovery](https://nlnet.nl/discovery/), part of the [Next Generation
  Internet](https://ngi.eu) initiative.
