<?php

namespace Drupal\reader\Form;

use Drupal\comment\CommentForm;

/**
 * Form handler for the reader edit forms.
 *
 * @internal
 */
class ReaderCommentForm extends CommentForm {

  /**
   * {@inheritdoc}
   */
  public function getOperation() {
    return 'reader';
  }

}
