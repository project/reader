<?php

namespace Drupal\reader\Form;

use Drupal\node\NodeForm;

/**
 * Form handler for the reader edit forms.
 *
 * @internal
 */
class ReaderNodeForm extends NodeForm {

  /**
   * {@inheritdoc}
   */
  public function getOperation() {
    return 'reader';
  }

}
