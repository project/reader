<?php

namespace Drupal\reader\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reader\ReaderInterface;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReaderSettingsUserForm extends FormBase {

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * ReaderSettingsUserForm constructor
   *
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   */
  public function __construct(UserDataInterface $user_data) {
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reader_settings_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->userData->get('reader', $this->currentUser()->id(), 'settings');
    $default_timeline_options = ['' => $this->t('- None -')];
    foreach (reader_get_implementators() as $module) {
      $function = $module . '_reader_channels';
      if (function_exists($function)) {
        $data = $function();
        if (!empty($data['channels'])) {
          $channels = [];
          foreach ($data['channels'] as $c) {
            $channels[$module . ReaderInterface::SEPARATOR . $c->uid] = $c->name;
          }
          if (!empty($channels)) {
            $label = (string) $data['label'];
            $default_timeline_options[$label] = $channels;
          }
        }
      }
    }

    $form['general'] = [
      '#type' => 'container',
    ];

    $default_timeline = !empty($settings['default_timeline']) ? $settings['default_timeline'] : '';
    $form['general']['default_timeline'] = [
      '#type' => 'select',
      '#title' => $this->t('Default timeline'),
      '#default_value' => $default_timeline,
      '#options' => $default_timeline_options,
    ];

    $pwa_display = !empty($settings['pwa_display']) ? $settings['pwa_display'] : 'standalone';
    $form['general']['pwa_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Display for the PWA'),
      '#default_value' => $pwa_display,
      '#options' => [
        'standalone' => $this->t('Standalone'),
        'fullscreen' => $this->t('Fullscreen'),
        'minimal-ui' => $this->t('Minimal-UI'),
        'browser' => $this->t('Browser'),
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = [
      'default_timeline' => $form_state->getValue('default_timeline'),
      'pwa_display' => $form_state->getValue('pwa_display')
    ];
    $this->userData->set('reader', $this->currentUser()->id(), 'settings', $settings);
    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
