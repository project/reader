<?php

namespace Drupal\reader\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserPasswordForm;

/**
 * Provides the user password form.
 */
class ReaderUserPasswordForm extends UserPasswordForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $_SESSION['use_reader_theme'] = TRUE;
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('reader.login');
  }

}
