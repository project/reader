<?php

namespace Drupal\reader;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 *
 */
abstract class ReaderBase implements ReaderInterface {

  /**
   * {@inheritdoc}
   */
  public function getSourcesPage($op) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTimelineActions($id) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function doTimelineAction($action, $id) {}

  /**
   * {@inheritdoc}
   */
  public function getPostActions($id, $item) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function doPostAction($action, $id, $items) {}

  /**
   * Add a sources button.
   *
   * @param $text
   * @param $module
   * @param string $op
   *
   * @return array[]
   */
  protected function addSourcesButton($text, $module, $op = 'list') {
    return [
      'button' => [
        '#markup' => Link::fromTextAndUrl($text, Url::fromRoute('reader.sources', [
          'module' => $module,
          'op' => $op,
        ], ['attributes' => ['class' => ['column-link']]]))->toString(),
      ],
    ];
  }

  /**
   * Returns the sources confirm delete text.
   *
   * @param $label
   *
   * @return string
   */
  protected function addSourcesConfirmDeleteText($label) {
    return '<div class="reader-sources-confirm-delete">' . $this->t('Are you sure you want to delete "@label"?', ['@label' => $label]) . '</div>';
  }

  /**
   * Returns the sources list url.
   *
   * @param $module
   *
   * @return \Drupal\Core\GeneratedUrl|string
   */
  protected function getSourcesListUrl($module) {
    return Url::fromRoute('reader.sources', ['module' => $module])->toString();
  }

}
