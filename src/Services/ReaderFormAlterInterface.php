<?php

namespace Drupal\reader\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;

interface ReaderFormAlterInterface {

  /**
   * Alter the node form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function alterNodeForm(array &$form, FormStateInterface $form_state, EntityInterface $entity);

  /**
   * Alter the comment form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function alterCommentForm(array &$form, FormStateInterface $form_state, EntityInterface $entity);

  /**
   * Alter the user login form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function alterLoginForm(array &$form, FormStateInterface $form_state);

  /**
   * Alter the user reset password form.
   *
   * @param array $form
   */
  public function alterResetPasswordForm(array &$form);

}
