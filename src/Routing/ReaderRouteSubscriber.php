<?php

namespace Drupal\reader\Routing;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 *
 * Class ReaderRouteSubscriber.
 *
 * @package Drupal\reader\Routing
 */
class ReaderRouteSubscriber extends RouteSubscriberBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ReaderRouterSubscriber constructor
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Overrides user.login route with our custom login form.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change path of user login to overridden TFA login form if the tfa module
    // exists.
    if ($this->moduleHandler->moduleExists('tfa') && ($route = $collection->get('reader.login'))) {
      $route->setDefault('_form', '\Drupal\tfa\Form\TfaLoginForm');
    }
  }

}
