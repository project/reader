<?php

namespace Drupal\reader\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Use 'reader_theme' theme.
 */
class ReaderThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    if ($route && $route->hasOption('_reader_theme')) {
      return TRUE;
    }

    if (isset($_GET['destination']) && strpos($_GET['destination'], 'reader/user/login') !== FALSE) {
      return TRUE;
    }

    if ($route_match->getRouteName() == 'user.reset.form' && isset($_SESSION['use_reader_theme'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    if ($route && $route->hasOption('_reader_theme')) {
      return $route->getOption('_reader_theme');
    }

    return 'reader_theme';
  }

}
