<?php

namespace Drupal\reader;

/**
 *
 */
interface ReaderInterface {

  const SEPARATOR = '::';

  /**
   * Returns a list of channels.
   *
   * @See https://indieweb.org/Microsub-spec#Channels_2 to format the 'channels'
   * key of this return.
   *
   * [
   *   'label' => 'My label',
   *   'channels' => [],
   * ]
   *
   * @return array
   */
  public function getChannels();

  /**
   * Get a sources page.
   *
   * @param $op
   *   Op usually is 'list', 'add', 'edit' or 'delete'. The list usually returns
   *   the list of sources. A simple 'add' form can also be returned here if
   *   wanted. That's entirely up to the implementor.
   *
   *   For inspiration, look at \Drupal\reader\Services\Reader
   *
   * @return mixed
   */
  public function getSourcesPage($op);

  /**
   * Get the timeline.
   *
   * @param $id
   *   The channel id to view.
   * @param $search
   *   The search terms.
   *
   * @see https://indieweb.org/Microsub-spec#Retrieve_Entries_in_a_Channel to
   * format the response.
   *
   * @return array
   */
  public function getTimeline($id, $search = NULL);

  /**
   * Returns a list of timeline actions.
   *
   * @param $id
   *   The channel id to get the actions for.
   *
   * @return array
   */
  public function getTimelineActions($id);

  /**
   * Do a timeline action.
   *
   * @param $action
   * @param $id
   *
   * @return mixed
   */
  public function doTimelineAction($action, $id);

  /**
   * Returns a list of post actions.
   *
   * @param $id
   *   The channel id to get the actions for.
   * @param $item
   *   The post item to get the actions for.
   *
   * @return array
   */
  public function getPostActions($id, $item);

  /**
   * Do a post action.
   *
   * @param $action
   * @param $id
   * @param $items
   *
   * @return mixed
   */
  public function doPostAction($action, $id, $items);

}
